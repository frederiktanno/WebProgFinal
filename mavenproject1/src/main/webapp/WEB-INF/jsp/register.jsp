<%-- 
    Document   : register
    Created on : Apr 3, 2017, 5:29:35 PM
    Author     : ASUS
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<%--<c:url var="addAction" value="/categories/add"></c:url>
<c:url var="removeAction" value="/categories/delete"></c:url>
<c:url var="updateAction" value="/categories/update"></c:url>
<c:url var="categoriesList" value="categories/view"></c:url>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration</title>
    </head>
    <body>
        <div align="center">
            <form:form action="register" method="post" commandName="userForm">
                <table border="0">
                    <tr>
                        <td colspan="2" align="center"<h2>register</h2></td>
                    </tr>
                    <tr>
                        <td >Username: </td>
                        <td><form:input path="username"/></td>
                    </tr>
                    <tr>
                    <td>Password:</td>
                    <td><form:password path="password" /></td>
                </tr>
                <tr>
                    <td>E-mail:</td>
                    <td><form:input path="email" /></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Register" /></td>
                </tr>
                </table>
            </form:form>
        </div>
    </body>
</html>
