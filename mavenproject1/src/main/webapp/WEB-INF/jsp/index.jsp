<%@page contentType="text\html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<c:set var="cp" value="${pageContext.request.servletContext.contextPath}" scope="request"/>
<c:url var="addAction" value="/categories/add"></c:url>
<c:url var="removeAction" value="/categories/delete"></c:url>
<c:url var="updateAction" value="/categories/update"></c:url>
<c:url var="categoriesList" value="categories/view"></c:url>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Spring 4 Web MVC via annotation</title>
        <link rel="stylesheet" type="text/css" href="${cp}/resources/css/style.css"/>
        <script src="${cp}/resources/js/js.js"></script>
    </head>
    <body>
      
    <h3>Users List</h3>
    <table class="tg">
        <tr>
            <th width="100">
                ID
            </th>
            <th width="100">
                Category
            </th>
            <th width="100">
                Item
            </th>

            <th width="100">
                Edit
            </th>
            <th width="100">
                Delete
            </th>
        </tr>
        <c:forEach items="${listcategories}" var="cat">
            <tr>
                <td>
                    ${cat.id}
                </td>
                 <td>
                    ${cat.category_name}
                </td>
                
                 <td>
                     <a href="<c:url value='${updateAction}/${cat.id}'/>">Edit</a>
                </td>
                 <td>
                    <a href="<c:url value='${removeAction}/${cat.id}'/>">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    </body>
</html>