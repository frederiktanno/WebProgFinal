/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;
import java.io.Serializable;
//import javax.persistence.Access;
//import javax.persistence.AccessType;
//import com.enterprise.services.UserServices;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
/**
 *
 * @author ASUS
 */
@Entity
@Table(name="\"users\"")
public class Users implements Serializable {
    @Id
    @Column(name="id")
     @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name="username")
    private String username;
    public String getUsername(){
        return username;
    }
    
    public void setUsername(String username){
        this.username=username;
    }
    
    @Column(name="email")
    private String email;
    public String getEmail(){
        return email;
    }
    
    public void setEmail(String email){
        this.email=email;
    }
    
    @Column(name="password")
    private String password;
    public String getPassword(){
        return password;
    }
    
    public void setPassword(String password){
        this.password=password;
    }
    
    @Override
    public String toString(){
        return(this.id + this.username + this.email);
    }
}
