/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.models;
import java.io.Serializable;
//import java.util.HashSet;
//import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//import javax.persistence.JoinTable;
//import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
//import javax.persistence.JoinTable;
//import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.ManyToOne;
/**
 *
 * @author ASUS
 */
//private Set<Categories>categories=new HashSet<>(0);//many-to-many without added columns
@Entity
@Table(name="\"items\"")
@Access(AccessType.FIELD)
public class Items implements Serializable{
    
    
    
    @Id
    @Column(name="idItem")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @Column(name="item_name",nullable=false)
    private String item;
    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
    
    //private Set<Categories>category=new HashSet<>(0);
  //  @ManyToOne(fetch=FetchType.LAZY,cascade=CascadeType.ALL,optional=false)
    
    /*@JoinTable(name="itemcategory",catalog="webprogquiz",joinColumns={
        @JoinColumn(name="idItem",nullable=false,updatable=false)
    },inverseJoinColumns={@JoinColumn(name="idCategory",nullable=false,updatable=false)})*/
   // @JoinColumn(name="idCategory");
    //@JsonIgnore
    
    @ManyToOne(optional = false)//,fetch=FetchType.LAZY,cascade=CascadeType.ALL)
    @JoinColumn(name="idCategory")
    private Categories category;
    
    public Categories getCategory() {
		return this.category;
	}

	public void setCategory(Categories category) {
		this.category = category;
	}
    @Override
    public String toString()
    {
        return (this.id + this.item);
    }
    
    
}
