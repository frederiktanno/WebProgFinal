/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.enterprise.models.Users;
import java.util.List;
/**
 *
 * @author ASUS
 */
@Repository("UserDao")
public class UsersDaoImpl extends AbstractDao implements UserDao{
    @Override
    @Transactional
    public void saveData(Users data){
        persist(data);
    }
    @Override
    public List<Users>getAllData(){
        Criteria c=getSession().createCriteria(Users.class);
        return(List<Users>)c.list();
    }
    @Override
    public Users findByName(String user){
        Criteria criteria=getSession().createCriteria(Users.class);
        criteria.add(Restrictions.eq("username", user));
        return (Users)criteria.uniqueResult();
    }
}
