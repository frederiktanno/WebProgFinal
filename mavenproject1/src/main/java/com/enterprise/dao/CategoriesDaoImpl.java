/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;
import java.util.List;
import com.enterprise.models.Categories;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Repository("categoriesDao")
public class CategoriesDaoImpl extends AbstractDao implements CategoriesDao {
    @Override
    @Transactional
    public void saveCategories(Categories catname){
        persist(catname);//abstractDao
    }
    @Override
    
    public List<Categories> findAllCategories(){
        Criteria criteria=getSession().createCriteria(Categories.class);
        return (List<Categories>)criteria.list();
    }
    @Override
    @Transactional
    public void deleteCategoryById(String id){
        Query query=getSession().createSQLQuery("DELETE from `category` WHERE `idCategory`=:id");
        query.setString("id", id);
        query.executeUpdate();
    }
    
   @Override
   public Categories findById(int id){
       Criteria criteria=getSession().createCriteria(Categories.class);
       criteria.add(Restrictions.eq("id", id));
        return (Categories) criteria.uniqueResult();
   }
   
   @Override
   @Transactional
   public void updateCategories(Categories catname){
       getSession().update(catname);
   }
   
   @Override
   public Categories findByName(String category){
       Criteria criteria=getSession().createCriteria(Categories.class);
       criteria.add(Restrictions.eq("category", category));
                                     //^same name as the variable to retrieve 'category
                                     //column in Category model
        return (Categories) criteria.uniqueResult();
   }
}
