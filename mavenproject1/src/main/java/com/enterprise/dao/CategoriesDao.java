/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;
import com.enterprise.models.Categories;
import java.util.List;
/**
 *
 * @author ASUS
 */
public interface CategoriesDao {
    void saveCategories(Categories catname);
    List<Categories> findAllCategories();
    void deleteCategoryById(String id);
    Categories findById(int id);
    void updateCategories(Categories catname);
    Categories findByName(String category);
}
