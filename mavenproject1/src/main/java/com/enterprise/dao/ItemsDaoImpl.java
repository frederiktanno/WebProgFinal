/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;
import com.enterprise.models.Categories;
import java.util.List;
import com.enterprise.models.Items;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Repository("ItemsDao")
public class ItemsDaoImpl extends AbstractDao implements ItemsDao{
    @Override
    @Transactional
    public void saveItems(Items items){
        persist(items);
    }
    @Override
    @SuppressWarnings("unchecked")
    public List<Items>findAllItems(){
     Criteria criteria=getSession().createCriteria(Items.class);
        return (List<Items>)criteria.list();
    }
    @Override
    @Transactional
    public void deleteItemById(String id){
        Query query=getSession().createSQLQuery("DELETE from `items` WHERE `idItem`=:id");
        query.setString("id", id);
        query.executeUpdate();
    }
    @Override
    public Items findById(int id){
        Criteria criteria=getSession().createCriteria(Items.class);
       criteria.add(Restrictions.eq("id", id));
        return (Items) criteria.uniqueResult();
    }
    @Override
    @Transactional
    public void updateItems(Items items){
        getSession().update(items);
    }
    
    @Override
    public Items findByName(String item){
        Criteria criteria=getSession().createCriteria(Items.class);
        criteria.add(Restrictions.eq("item", item));
        return (Items)criteria.uniqueResult();
    }
    @Override
    @Transactional
    public void saveCategories(Categories catname){
        //boolean con=searchCategories(catname);
        //Categories catId;
        //if(con==false){
            persist(catname);//abstractDao
        //}
        //else{
            
        //}
    }
    
    /*@Override
    public boolean searchCategories(Categories catname){
        Criteria criteria=getSession().createCriteria(Categories.class);
        List<Categories>catList=criteria.list();
        if(catList.contains(catname)){
            return true;
        }
        else
            return false;
    }*/
}
