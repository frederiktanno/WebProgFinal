/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;
import java.util.List;
import com.enterprise.models.Users;
/**
 *
 * @author ASUS
 */
public interface UserDao {
    void saveData(Users data);
    List<Users>getAllData();
    Users findByName(String name);
}
