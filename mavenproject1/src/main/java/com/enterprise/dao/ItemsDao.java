/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.dao;
import com.enterprise.models.Items;
import com.enterprise.models.Categories;
import java.util.List;
/**
 *
 * @author ASUS
 */
public interface ItemsDao {
    void saveItems(Items items);
    List<Items>findAllItems();
    void deleteItemById(String id);
    Items findById(int id);
    void updateItems(Items items);
    Items findByName(String item);
    void saveCategories(Categories category);

    //boolean searchCategories(Categories catname);
}
