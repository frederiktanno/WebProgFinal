/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import com.enterprise.models.Categories;
import com.enterprise.services.CategoriesServices;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;
/**
 *
 * @author ASUS
 */
@CrossOrigin(origins="*",maxAge=3600)
@Controller
@ComponentScan("com.enterprise.services")
public class CategoryController {
    @Autowired
        private CategoriesServices categoriesServices;
        /*@RequestMapping(value="/categories",method=RequestMethod.GET)
        public String test(ModelMap models){
            List<Categories>categories=categoriesServices.findAllCategories();
            models.put("listcategories", categories);
            System.out.println(categories.toString());
            return "index";
        }*/
    
        //get all categories
        @RequestMapping(value="/categories",method=RequestMethod.GET)
        public ResponseEntity<List<Categories>>viewCategories(ModelMap models){
            List<Categories>categories=categoriesServices.findAllCategories();
            if(categories.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(categories,HttpStatus.OK);
        }
        
        //insert new category
        @RequestMapping(value="/categories",method=RequestMethod.POST)
        public ResponseEntity<Void>addCategories(@RequestBody Categories categories,UriComponentsBuilder ucBuilder){
            System.out.println("Adding category "+categories.getCategory());
            categoriesServices.saveCategories(categories);
            HttpHeaders header=new HttpHeaders();
            return new ResponseEntity<>(header,HttpStatus.CREATED);
        }
        
        //search for a category
        @RequestMapping(value="/categories/{category_name}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
        public ResponseEntity<Categories>getCategory(@PathVariable("category_name")String category){
            Categories categories=categoriesServices.findByName(category);
            if(categories==null){
                System.out.println("Category not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(categories,HttpStatus.OK);
        }
        
        //update category
        @RequestMapping(value="/categories/{id}",method=RequestMethod.PUT)
        public ResponseEntity<Categories>updateCategory(@PathVariable("id")String id, @RequestBody Categories category){
            System.out.println("Updating category");
            Categories selected=categoriesServices.findById(id);
            if(selected==null){
                System.out.println("Category not found");
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            selected.setCategory(category.getCategory());
            categoriesServices.updateCategories(selected);
            return new ResponseEntity<>(selected,HttpStatus.OK);
        }
        
        //delete category
        @RequestMapping(value="/categories/{id}",method=RequestMethod.DELETE)
        public ResponseEntity<Categories> removeCategory(@PathVariable("id")String id){
            Categories category=categoriesServices.findById(id);
            if(category==null){
                System.out.println("Category not found");
                return new ResponseEntity<Categories>(HttpStatus.NOT_FOUND);
            }
            categoriesServices.deleteCategoryById(id);
            return new ResponseEntity<Categories>(category,HttpStatus.OK);
        }
}
