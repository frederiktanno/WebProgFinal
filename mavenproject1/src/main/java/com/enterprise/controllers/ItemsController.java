/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;
import com.enterprise.models.Categories;
import com.enterprise.models.Items;
import com.enterprise.services.ItemServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;
/**
 *
 * @author ASUS
 */
@CrossOrigin(origins="*",maxAge=3600)
@Controller
@ComponentScan("com.enterprise.services")
public class ItemsController {
    @Autowired
    private ItemServices itemServices;
    @RequestMapping(value="/items",method=RequestMethod.GET)
    public ResponseEntity<List<Items>>viewCategories(ModelMap models){
            List<Items>items=itemServices.findAllItems();
            if(items.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(items,HttpStatus.OK);
        }
    
    //insert new item
    @RequestMapping(value="/items",method=RequestMethod.POST)
    public ResponseEntity<Void>addItems(@RequestBody Map<String,Object>data,UriComponentsBuilder ucBuilder){
            //System.out.println("Adding item "+item.getItem());
            //System.out.println("Adding category"+category.getCategory());
            //itemServices.saveItems(item);
            itemServices.insertItemCategories(data.get("item").toString(), data.get("category").toString());
            //itemServices.saveCategories(category);
            HttpHeaders header=new HttpHeaders();
            return new ResponseEntity<>(header,HttpStatus.CREATED);
        }
    //update item
    @RequestMapping(value="/items/{id}",method=RequestMethod.PUT)
    public ResponseEntity<Items>updateItems(@PathVariable("id")String id, @RequestBody Items item){
        System.out.println("Updating item");
        Items selected=itemServices.findById(id);
        if(selected==null){
                System.out.println("Item not found");
                return new ResponseEntity<Items>(HttpStatus.NOT_FOUND);
            }
        selected.setItem(item.getItem());
        itemServices.updateItems(selected);
        return new ResponseEntity<Items>(selected,HttpStatus.OK);
    }
    
    //search item
    @RequestMapping(value="/items/{item_name}",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Items>getItems(@PathVariable("item_name")String item_name){
        Items item=itemServices.findByName(item_name);
        if(item==null){
                System.out.println("Item not found");
                return new ResponseEntity<Items>(HttpStatus.NOT_FOUND);
            }
        return new ResponseEntity<Items>(item,HttpStatus.OK);
    }
    
    //delete item
    @RequestMapping(value="/items/{id}",method=RequestMethod.DELETE)
    public ResponseEntity<Items>deleteItems(@PathVariable("id")String id){
        Items item=itemServices.findById(id);
        if(item==null){
                System.out.println("Item not found");
                return new ResponseEntity<Items>(HttpStatus.NOT_FOUND);
            }
        itemServices.deleteItemById(id);
        return new ResponseEntity<Items>(item,HttpStatus.OK);
    }
}
