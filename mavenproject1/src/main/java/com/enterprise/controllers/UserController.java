/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.controllers;

import com.enterprise.models.Users;
import com.enterprise.services.UserServices;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;
/**
 *
 * @author ASUS
 */
@CrossOrigin(origins={"*"},maxAge=3600)
@Controller
@ComponentScan("com.enterprise.services")
public class UserController {
    
    /*@RequestMapping(value="/register",method=RequestMethod.POST)
    public String addData(@RequestBody Map<String,Object>data,UriComponentsBuilder ucBuilder){
        uServices.insertData(data.get("username").toString(), data.get("email").toString(), data.get("password").toString());
        HttpHeaders header=new HttpHeaders();
        return "register";
    }*/
    @Autowired
    private UserServices uServices;
    @RequestMapping(value="/register",method=RequestMethod.POST)
    public ResponseEntity<Void> addData(@ModelAttribute("userForm")Users user,Map<String,Object>model){
        uServices.insertData(user);
        HttpHeaders header=new HttpHeaders();
        return new ResponseEntity<>(header,HttpStatus.CREATED);
    }
    
    /*@RequestMapping(value="/register",method=RequestMethod.GET)
    public ResponseEntity<List<Users>> view(Map<String,Object>model){
        Users userForm=new Users();
        model.put("userForm", userForm);
        return "register";
    }*/

    @RequestMapping(value="/user",method=RequestMethod.GET)
    public ResponseEntity<List<Users>>viewUsers(ModelMap models){
        List<Users>users=uServices.getAllData();
        if(users.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(users,HttpStatus.OK);
    }
    
    @RequestMapping(value="/user",method=RequestMethod.POST)
    public ResponseEntity<Void>addUsers(@RequestBody Users user,UriComponentsBuilder ucBuilder){
        uServices.insertData(user);
        HttpHeaders header=new HttpHeaders();
        return new ResponseEntity<>(header,HttpStatus.CREATED);
    }
}
