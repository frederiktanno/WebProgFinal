/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;
import com.enterprise.models.Categories;
import com.enterprise.models.Items;
import java.util.List;

/**
 *
 * @author ASUS
 */
public interface ItemServices {
    void saveItems(Items items);
    List<Items>findAllItems();
    void deleteItemById(String id);
    Items findById(String id);
    void updateItems(Items items);
    Items findByName(String item);
    void saveCategories(Categories category);
    void saveItemCategory(Items item);
    void insertItemCategories(String item, String category);
}
