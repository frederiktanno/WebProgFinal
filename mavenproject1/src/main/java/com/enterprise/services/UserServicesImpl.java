/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import com.enterprise.models.Users;
import com.enterprise.dao.UserDao;
/**
 *
 * @author ASUS
 */
@Service("UserServices")
@Transactional
public class UserServicesImpl implements UserServices {
    @Autowired
    public UserDao uDao;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Override
    public void saveData(Users data){
        uDao.saveData(data);
    }
    @Override
    public List<Users>getAllData(){
        return uDao.getAllData();
    }
    @Override
    public void insertData(Users user){
        /*Users u=new Users();
        u.setUsername(username);
        u.setPassword(password);
        u.setEmail(email);
        uDao.saveData(u);*/
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        uDao.saveData(user);
    }
    @Override
    public Users findByName(String name){
        return uDao.findByName(name);
    }
}
