/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;
import com.enterprise.dao.ItemsDao;
import com.enterprise.dao.CategoriesDao;
import com.enterprise.models.Items;
import com.enterprise.models.Categories;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Service("itemServices")
@Transactional
public class ItemServicesImpl implements ItemServices {
    @Autowired
    public ItemsDao dao;
    @Autowired
    public CategoriesDao cdao;
    @Override
    public void saveItems(Items items){
        dao.saveItems(items);
    }
    @Override
    public List<Items>findAllItems(){
        return dao.findAllItems();
    }
    @Override
    public void deleteItemById(String id){
        dao.deleteItemById(id);
    }
    @Override
    public Items findById(String id){
        return dao.findById(Integer.parseInt(id));
    }
    @Override
    public void updateItems(Items items){
        dao.updateItems(items);
    }
    @Override
    public Items findByName(String item){
        return dao.findByName(item);
    }
    @Override
    public void saveCategories(Categories category){
        cdao.saveCategories(category);
    }
    @Override
    public void saveItemCategory(Items item){
        
        cdao.saveCategories(item.getCategory());
        dao.saveItems(item);
    }
    @Override
    public void insertItemCategories(String item, String category){
        Categories cat=new Categories();
        cat.setCategory(category);
        cdao.saveCategories(cat);
        
        Items itemObj=new Items();
        itemObj.setCategory(cat);
        itemObj.setItem(item);
        dao.saveItems(itemObj);
    }
}
