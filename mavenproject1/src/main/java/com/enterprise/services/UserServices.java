/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;

/**
 *
 * @author ASUS
 */
import java.util.List;
import com.enterprise.models.Users;
public interface UserServices {
    void saveData(Users data);
    List<Users>getAllData();
    void insertData(Users user);
    Users findByName(String name);
}
