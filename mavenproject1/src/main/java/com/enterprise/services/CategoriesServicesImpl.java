/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.enterprise.services;
import com.enterprise.dao.CategoriesDao;
import com.enterprise.models.Categories;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Service("categoriesServices")
@Transactional
public class CategoriesServicesImpl implements CategoriesServices {
    @Autowired
    public CategoriesDao dao;
    @Override
    public void saveCategories(Categories catname){
        dao.saveCategories(catname);
    }
    @Override
    public List<Categories>findAllCategories(){
        return dao.findAllCategories();
    }
    @Override
    public void deleteCategoryById(String id){
        dao.deleteCategoryById(id);
    }
    @Override
    public Categories findById(String id){
        return dao.findById(Integer.parseInt(id));
    }
    @Override
    public void updateCategories(Categories catname){
        dao.updateCategories(catname);
    }
    @Override
    public Categories findByName(String category){
        return dao.findByName(category);
    }
}
